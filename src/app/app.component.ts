import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  peopleCount: number;
  bulbs = [];
  turnedOnBulbCount = 0;

  getBulbStatus(index) {
    // only squre unmbers are turning back on
    // reference: http://puzzles.nigelcoldwell.co.uk/six.htm
    return Math.sqrt(index) % 1 === 0 ? 'on' : 'off';
  }

  onBulbCounted(count) {
    this.bulbs = []; // reset
    this.turnedOnBulbCount = 0; // reset

    for (let i = 1; i <= count; i++) {
      const bulbStatus = this.getBulbStatus(i);

      this.bulbs.push({status: bulbStatus});

      if (bulbStatus === 'on') {
        this.turnedOnBulbCount++;
      }
    }
  }
}
