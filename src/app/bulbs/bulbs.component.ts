import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bulbs',
  templateUrl: './bulbs.component.html',
  styleUrls: ['./bulbs.component.scss']
})
export class BulbsComponent implements OnInit {
  @Input() bulb: {status: string};
  @Input() index: number;

  constructor() { }

  ngOnInit() {
  }
}
