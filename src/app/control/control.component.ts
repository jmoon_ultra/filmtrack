import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.scss']
})
export class ControlComponent implements OnInit {
  @Output() bulbCounted = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  onGetBulbCount(bulbCountInput: HTMLInputElement) {
    this.bulbCounted.emit(parseInt(bulbCountInput.value, 10));
  }
}
